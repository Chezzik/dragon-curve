#!/usr/bin/env python3

from PIL import Image, ImageDraw as D
in_file = "input.png"
out_file = "output.png"

i=Image.open(in_file)
draw=D.Draw(i)
draw.rectangle([(100,100),(250,250)],outline="white")
#i.show()
i.save(out_file)
