#!/usr/bin/env python3

from PIL import Image, ImageDraw as D

out_file = "output.png"
width = 9 # The width of the line (in pixels)
grid = 6 # The width of the grid between points
canvas_radius = 32                      # in points
canvas_diameter = 2 * canvas_radius     # in points
image_size = grid + (width + grid) * canvas_diameter
iterations=512

fractal=Image.new('RGB', (image_size, image_size), (255, 255, 255))
draw=D.Draw(fractal)
#colors = ("orange", "green", "pink", "blue")
colors = ((235, 177, 52),
          (172, 230, 80),
          (230, 110, 198),
          (85, 211, 250))

x_data = (1, 0, -1, 0) # The difference in x when moving forward 1 for each direction.
y_data = (0, -1, 0, 1) # The difference in y when moving forward 1 for each direction.

def draw_point(x, y, color):
    global draw
    x1 = grid + (width + grid) * x
    y1 = grid + (width + grid) * y
    x2 = x1 + width - 1
    y2 = y1 + width - 1
    draw.rectangle([(x1 - grid, y1 - grid), (x2 + grid, y2 + grid)], fill="black")
    draw.rectangle([(x1, y1), (x2, y2)], fill=colors[color])

def draw_line(x, y, xx, yy, color):
    x1 = grid + (width + grid) * min(x, xx)
    y1 = grid + (width + grid) * min(y, yy)
    x2 = grid + (width + grid) * max(x, xx) + width - 1
    y2 = grid + (width + grid) * max(y, yy) + width - 1
    draw.rectangle([(x1, y1), (x2, y2)], fill=colors[color])
    
def draw_grid_line(x, y, dir):
    xx = grid + (width + grid) * x
    yy = grid + (width + grid) * y
    if (dir == 0):
        x1 = xx + width
        x2 = xx + width + grid - 1
        y1 = yy + width + grid - 1
        y2 = yy - grid
    if (dir == 1):
        x1 = xx - grid
        x2 = xx + width + grid - 1
        y1 = yy - grid
        y2 = yy -1
    if (dir == 2):
        x1 = xx - 1
        x2 = xx - grid
        y1 = yy + width + grid - 1
        y2 = yy - grid
    if (dir == 3):
        x1 = xx - grid
        x2 = xx + width + grid - 1
        y1 = yy + width
        y2 = yy + width + grid - 1

    draw.rectangle([(x1, y1), (x2, y2)], fill="black")
    
def index_is_left(i):
    while(i % 2 == 0):
        i = i >> 1
    return (False if (i & 2) else True)

def index_dir_change(i):
    d1 = index_is_left(i)
    d2 = index_is_left(i + 1)
    if (d1 == True and d2 == True):
        return -1
    if (d1 == False and d2 == False):
        return 1
    return 0

class Turtle:
    def __init__(self, color, direction, x=-1, y=-1):
        self.x = (x if x > 0 else canvas_radius)
        self.y = (y if y > 0 else canvas_radius)
        self.dir = direction
        self.c = color
        draw_point(self.x, self.y, color)
        self.index = 0
        self.forward(1)
    def turn_left(self):
        draw_grid_line(self.x, self.y, self.dir)
        self.dir = (self.dir + 1) % 4
    def turn_right(self):
        draw_grid_line(self.x, self.y, self.dir)
        self.dir = (self.dir + 3) % 4
    def forward(self, distance):
        x2 = self.x + x_data[self.dir] * distance
        y2 = self.y + y_data[self.dir] * distance
        draw_line(self.x, self.y, x2, y2, self.c)
        self.x = x2
        self.y = y2
        draw_grid_line(x2, y2, (self.dir + 1) % 4)
        draw_grid_line(x2, y2, (self.dir + 3) % 4)
    def run_iteration(self):
        self.index = self.index + 1
        dir_change = index_dir_change(self.index)
        if (dir_change == -1):
            self.turn_left()
        if (dir_change == 1):
            self.turn_right()
        self.forward(1)
    def iterate_to(self, target):
        while (self.index < target):
            self.run_iteration()
        draw_grid_line(self.x, self.y, self.dir)

def main():
    t0 = Turtle(0, 0, canvas_radius + 0, canvas_radius + 0)
    t1 = Turtle(1, 1, canvas_radius + 0, canvas_radius - 1)
    t2 = Turtle(2, 2, canvas_radius - 1, canvas_radius - 1)
    t3 = Turtle(3, 3, canvas_radius - 1, canvas_radius + 0)
    t0.iterate_to(iterations)
    t1.iterate_to(iterations)
    t2.iterate_to(iterations)
    t3.iterate_to(iterations)

if __name__ == "__main__":
    main()

#fractal.show()
fractal.save(out_file)
